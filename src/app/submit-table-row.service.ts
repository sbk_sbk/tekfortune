import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SubmitTableRowService {

  constructor(private httpClient: HttpClient) { }

  // rest api api/submit post call
  postDataRow(postData): Observable<any> {
    return this.httpClient.post('/api/submit', postData).pipe(
      catchError(err => {
        console.log('Handling error locally and rethrowing it...', err);
        return throwError(err);
    }));
  }
}
