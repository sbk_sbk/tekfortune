import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { error } from 'protractor';
import { SubmitTableRowService } from './submit-table-row.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  title = 'tekfortune-assignment';
  tableData: any = [];
  config: any = [];
  tableHeader: Array<any> = [];
  listDropDown = [50, 100, 200];

  constructor(private httpClient: HttpClient, private submitTableRow: SubmitTableRowService) {
    this.config = {
      itemsPerPage: 50,
      currentPage: 1
    };

  }
    ngOnInit() {
      // read json file data to display header and table data
      this.httpClient.get( 'assets/sample_data.json' ).subscribe( data => {
        this.tableData = data;
        this.config.totalItems =  this.tableData.length;
        this.tableHeader = Object.keys(data[0]);
        if (this.tableHeader.indexOf('id') > 0) {
          this.tableHeader.splice(this.tableHeader.indexOf('id'), 1);
          this.tableHeader.unshift('id');
      }
    });
  }

  // function for selection of number rows
  listNumberRecords(event) {
    this.config.itemsPerPage = event.target.value;
  }

  // pagination next function
  nextPage(event) {
    this.config.currentPage = event;
  }

  // function to post each data row
  postDataRow(tableData) {
    const postData = {
      id: tableData.id,
      status: tableData.status
    };
    this.submitTableRow.postDataRow(postData).subscribe(response => {});

  }
}
