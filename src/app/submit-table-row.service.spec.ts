import { TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { SubmitTableRowService } from './submit-table-row.service';

describe('SubmitTableRowService', () => {
  let service: SubmitTableRowService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });
    service = TestBed.inject(SubmitTableRowService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
